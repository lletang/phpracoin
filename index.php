<?php
session_start();


use controllers\AnnonceController as Annonce;
use controllers\UserController as User;
use controllers\AdminController as Admin;
use \confs\Connection;
use \models\photoAnnonce as Photo;
use \models\annonce as a;
use \models\categorie as c;
define("INSTALLATION_DIR","template");
require 'vendor/autoload.php';

$app = new \Slim\Slim();

if(!preg_match("#api#", $_SERVER['REQUEST_URI']))
    $app->add(new \Slim\Extras\Middleware\CsrfGuard());


$app->get( '/',function () {
    $controller = new Annonce();
    $controller->affiche();
});

$app->get( '/annonces/mesAnnonces',function () {
        $controller = new Annonce();
        $controller->afficheMesAnnonces();
});

$app->get('/annonces/mesAnnonces/:id',function ($id) {
        $controller = new Annonce();
        $controller->afficheMesAnnonces($id);
});


$app->get('/annonces/new',function () {
   
        $controller = new Annonce();
        $controller->add();
});

$app->get('/annonce/search',function () {
        $controller = new Annonce();
        $controller->search();
});


$app->get('/annonces',function () {
       $controller = new Annonce();
        $controller->affiche();
    
});


$app->post('/annonces/ajoute',function () {
        $controller = new Annonce();
        $controller->ajoute();

})->name("ajoutAnnonce");


$app->post('/annonces/ajoute/:id',function ($id) {
        $controller = new Annonce();
        $controller->ajoute($id);

});

$app->get('/annonces/:id',function ($id) {
        $controller = new Annonce();
        $controller->affiche($id);
});

$app->post('/annonces/:id',function ($id) {
        $controller = new Annonce();
        $controller->affiche($id);
});


$app->get('/annonces/delete/:id',function ($id) {
    
        $controller = new Annonce();
        $controller->delete($id);
})->name("supprimerAnnonce");

$app->post('/annonces/delete/:id',function ($id) {
    
        $controller = new Annonce();
        $controller->delete($id);
});


$app->get('/annonces/edit/:id',function ($id) {
        $controller = new Annonce();
        $controller->edit($id);
});

$app->post('/annonces/edit/:id',function ($id) {
        $controller = new Annonce();
        $controller->edit($id);
});




$app->get('/user/inscription',function () {
    
        $controller = new User();
        $controller->view();
    })->name("inscriptions");

$app->post("/user/inscription",function(){

    $controller = new User();
    $controller->add();


})->name("ajoutInscriptions");







$app->get('/user/profil',function () {
    
        $controller = new User();
        $controller->view();
    })->name("profils");



$app->post("/user/profil",function(){

    $controller = new User();
    $controller->add();


})->name("modifProfil");






	
/* pour afficher la page de connection */
$app->get('/user/connection', function() {
		$controller = new User();
		$controller->connection();
	});
	
/* pour verifier la connection */
$app->post('/user/connection', function() {
		$controller = new User();
		$controller->check();
	});

/* pour se deconecter */
$app->get('/user/logout', function() {
		$controller = new User();
		$controller->logout();
	});

$app->get('/user/profil/:id', function($id) {
        $controller = new Admin();
        $controller->viewUser($id);
});
	
/* pour aller sur la page de gestion utilisateur pour admin*/
$app->get('/admin/gestionuser', function() {
		$controller = new Admin();
		$controller->afficherForm();
	});

/* pour ajouter un user par admin*/
$app->post('/admin/gestionuser', function() {
		$controller = new Admin();
		$controller->adduser();
	});	
	
/* pour aller sur la page de gestion categorie pour admin*/
$app->get('/admin/gestioncate', function() {
		$controller = new Admin();
		$controller->afficherGestionCate();
	});
	
/* ajouter une categorie pour admin*/
$app->post('/admin/gestioncate/add', function() {
		$controller = new Admin();
		$controller->addcate();
	});
	
/* supprimer une categorie pour admin*/
$app->post('/admin/gestioncate/supp', function() {
		$controller = new Admin();
		$controller->suppcate();
	});

/* pour aller sur la page de gestion des anonce pour admin*/
$app->get('/admin/viewannonce', function() {
		$controller = new Admin();
		$controller->viewfreshannonce();
	});	
	

/* supprimer une annonce dans le preview*/
$app->get('/admin/viewannonce/delete/:id', function($id) {
		$controller = new Admin();
		$controller->deleteAdmin($id);
	});	


// API group
$app->group('/api', function () use ($app) {
 
    // Version group
    $app->group('/v1', function () use ($app) {

        // POST route 
        $app->post('/annonces', function () use ($app) {
            postAnnonce();
        });
 
        // GET route
        $app->get('/annonces', function () use ($app) {
            getAnnonces($app);
        });

        /* // PUT route, for updating
        $app->put('/annonces/:id', function ($id) use ($app) {
 
        });*/

        // GET filtre
        $app->get('/annonces/?', function () use ($app) {
            
            
            filtreAnnonces($app);
 
        });
 
        // GET route id
        $app->get('/annonces/:id', function ($id) use ($app) {

            getAnnonces($app,$id);
 
        })->name("apiVisuUrl");
 
        // DELETE route
        $app->delete('/annonces/delete/:id', function ($id) use ($app) {
 
        });

        // GET route
        $app->get('/categories', function () use ($app) {
            getCategories($app);
        });

        // GET route id
        $app->get('/categories/:id', function ($id) use ($app) {

            getCategories($app,$id);
 
        })->name("apiVisuUrl");
    });
});

$app->run();


function filtreAnnonces($app){
    Connection::makeConnexion();
    $nom_server = $_SERVER["SERVER_NAME"];


    if(!empty($app->request->get("categorie_id")) && $app->request->get("categorie_id")!=0)
                    $category = $app->request->get("categorie_id");
    else $category= false;

    if(!empty($app->request->get("region_id")))
        $reg = $app->request->get("region_id");
    else $reg = false;

    if(!empty($app->request->get("motCle")))
        $motcle = $app->request->get("motCle");
    else $motcle = false;


    if(!empty($app->request->get("prixMin")) && !empty($app->request->get("prixMax"))){

                    
        $prixmin = $app->request->get("prixMin");
        $prixmax = $app->request->get("prixMax");
                    
    }
    else{
        $prixmin = false;
        $prixmax = false;

    } 

    
            

                
    $annonces = a::where(function($query) use ($category, $reg, $prixmin, $prixmax,$motcle) {
        if($category){

            $query->where('categorie_id', $category);
        }

        if($reg){
                            
            $query->where('region_id', $reg);
        }
                        
        

        if($motcle){
                       
            $query->where('titre','LIKE',"%$motcle%")
            ->orWhere('descriptif','LIKE',"%$motcle%");

        }
        if($prixmin && $prixmax){
                      
            $query->whereBetween('prix', array($prixmin,$prixmax));
        }
                            

    })
    ->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
    ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
    ->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
    ->orderBy('date', 'DESC')
    ->get();

    $a=array();
        $i=0;
    
    foreach ($annonces as $ann) {
            
        $a[$i]["id"]= $ann->annonce_id;
        $a[$i]["titre"]= $ann->titre;
        $a[$i]["prix"]= $ann->prix;
        $a[$i]["date"]= $ann->date;
        $a[$i]["url_photo"]= $ann->url_photo;
        $a[$i]["libelle_categorie"]= $ann->libelle_categorie;
        $a[$i]["libelle_region"]= $ann->libelle_region;
        $a[$i]["links"]["rel"]= "self";
        $a[$i]["links"]["href"]= $nom_server."/api/v1/annonces/".$ann->annonce_id;
          
            
        $i++;
           
           
    }

    echo '{"annonces": ' . json_encode($a) . ',"links":{"rel":"self","href":"'.$nom_server."/api/v1/annonces".'"}}';

   
}


function getAnnonces($app,$id=NULL) {

    Connection::makeConnexion();
    $nom_server = $_SERVER["SERVER_NAME"];





try {

    if($id!=NULL)
    {
       
        $annonces = a::where('annonces.id',$id)
            ->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
            ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
            ->get();

      
       $a=array();
        $photos = Photo::where('annonce_id', '=', $id)->get();
        foreach ($annonces as $ann) {
        $a["id"]= $id;
            $a["titre"]= $ann->titre;
            $a["prix"]= $ann->prix;
            $a["date"]= $ann->date;
            $a["categorie"]["id"]= $ann->categorie_id;
            $a["categorie"]["libelle"]= $ann->libelle_categorie;
            $a["region"]["id"]= $ann->region_id;
            $a["region"]["libelle"]= $ann->libelle_region;
            $i=0;
            foreach ($photos as $photo) {
                $a["photos"][$i] = $nom_server."/".$photo->url_photo;
                $i++;
            }

            $a["links"]["rel"]= "self";
            $a["links"]["href"]= $nom_server."/api/v1/annonces/edit/".$id;

         }   

         echo json_encode($a) ;

    }
    else {


        $annonces = a::orderBy('date', 'DESC')
                ->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
                ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
                ->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
                ->groupBy("annonces.id")
                ->get();

    

   
        $a=array();
        $i=0;
     
            foreach ($annonces as $ann) {
            
            $a[$i]["id"]= $ann->annonce_id;
            $a[$i]["titre"]= $ann->titre;
            $a[$i]["prix"]= $ann->prix;
            $a[$i]["date"]= $ann->date;
            $a[$i]["url_photo"]= $ann->url_photo;
            $a[$i]["libelle_categorie"]= $ann->libelle_categorie;
            $a[$i]["libelle_region"]= $ann->libelle_region;
            $a[$i]["links"]["rel"]= "self";
            $a[$i]["links"]["href"]= $nom_server."/api/v1/annonces/".$ann->annonce_id;
          
            
            $i++;
           
           
        }

        echo '{"annonces": ' . json_encode($a) . ',"links":{"rel":"self","href":"'.$nom_server."/api/v1/annonces".'"}}';

    }

    }catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}


function getCategories($app,$id=NULL) {


    Connection::makeConnexion();
    try {
        if($id!=NULL) {
            $categorie = c::where('categories.id',$id)
            ->get();
            $c=array();
            $i=0;
            foreach ($categorie as $cat) {
                $c[$i]["id"]= $cat->id;
                $c[$i]["libelle"]= $cat->libelle_categorie;
                $c[$i]["links"]["rel"] = "self";
                $c[$i]["links"]["href"] = "categories/".$cat->id;
                ++$i;
            }
            echo  json_encode($c);
        }
        else {
            $categories = c::all();
            $c=array();
            $i=0;
            foreach ($categories as $cat) {
                $c[$i]["id"]= $cat->id;
                $c[$i]["libelle"]= $cat->libelle_categorie;
                $c[$i]["links"]["rel"] = "self";
                $c[$i]["links"]["href"] = "categories/".$cat->id;
                ++$i;
            }
            echo '{"categories": ' . json_encode($c) . '}';
        }
        
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    
}


function postAnnonce() {

    Connection::makeConnexion();
    $app = new \Slim\Slim();

    $data = $app->request->post();
    $dataImage = null;

    foreach ($_FILES as $key => $value) {

        if (!empty($_FILES[$key]['tmp_name'])) {
            $dataImage[$key] = $_FILES[$key];
        }
    }

    if (empty($dataImage) || $dataImage == null)
        $defaultUrlImg = './img/img.jpg';

    $v = a::validate($data, $dataImage);

    if (!empty($v["erreur"])) {
        $app->response->header('Content-Type', 'application/json;charset=utf-8');
        echo "<pre>"; echo json_encode($v["erreur"]);
    }
    else {

        $titre = $data['titre'];
        $descriptif = $data['descriptif'];
        $categorie = $data['categorie'];
        $telephone = $data['telephone'];
        $email = $data['email'];
        $prix = $data['prix'];
        if (!empty($data['password']))
            $mdp = $data['password'];

        $id = 0;

        if (!empty($_SESSION['user'])) {
            $id = $_SESSION['user']['id'];
        }

        $annonce = new a();
        $annonce->user_id = $id;
        
        $annonce->categorie_id = $categorie;
        if (!empty($mdp))       
            $annonce->mdp = password_hash($mdp, PASSWORD_DEFAULT);

        $annonce->telephone = htmlentities($telephone);
        $annonce->titre = htmlentities($titre);
        $annonce->prix = htmlentities($prix);
        $annonce->descriptif = htmlentities($descriptif);
        $annonce->email = htmlentities($email);
        $annonce->categorie_id = htmlentities($categorie);      
        $annonce->previsu = 0;
        $annonce->save();


        if (!empty($defaultUrlImg)) {
            $photo = new Photo();

            $photo->url_photo = $defaultUrlImg;
            $photo->annonce_id = $annonce->id;
            $photo->save();
        }
        else {
            foreach ($_FILES as $key => $value) {
                $name = $key.'_name';
                $$name = $_FILES[$key]["name"]; 
                $size = $key.'_size';
                $$size = $_FILES[$key]["size"];
                $tmpName = $key.'_tmp';
                $$tmpName = $_FILES[$key]["tmp_name"];
                $extension_upload = $key.'_ext';
                $$extension_upload = strtolower(substr(strrchr($$name, '.') ,1));
                $namePhoto = $key.'_rename';
                $$namePhoto = time().hash("sha256", $$tmpName).".".$$extension_upload;

                move_uploaded_file($$tmpName, sprintf('./upload/%s', $$namePhoto));

                $photo = $key.'_photo';
                $$photo = new Photo();

                $$photo->url_photo = "./upload/".$$namePhoto;
                $$photo->annonce_id = $annonce->id;
                $$photo->save();
            } 
        }
        
        $app->response->header('Content-Type', 'application/json;charset=utf-8');
        echo "<pre>"; echo json_encode("Success, announce added");
    }

}


<?php 

namespace views;
abstract class View {
	 protected $layout =null ;
	 protected $obj ;
	 protected $arrayVar ;
	 
	 public function __construct($o=null) { $this->obj = $o ; }
	 
	 public function addVar($var, $val) { $this->arrayVar[$var]=$val; }
	 
	 public function render() {
		 $loader = new \Twig_Loader_Filesystem(INSTALLATION_DIR);
		 $twig = new \Twig_Environment( $loader, array(
   		 'debug' => true,
		 ));
		 $twig->addExtension(new \Twig_Extension_Debug());
		 $twig->addFilter('var_dump', new \Twig_Filter_Function('var_dump'));
		 
		 
		 /* ajout de $_SESSION en global pour twig */
		 $twig->addGlobal("session", $_SESSION);

		 $tmpl = $twig->loadTemplate($this->layout);

		 return $tmpl->render($this->arrayVar);
	 }
	 
	 public function display() { echo $this->render() ; }
}
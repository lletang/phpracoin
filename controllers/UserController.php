<?php

namespace controllers;

use \confs\Connection;
use \models\user;
use \models\region;
use \models\categorie;
use \models\roles;
use \views;
use \models\annonce;


class UserController {

	function view(){
		Connection::makeConnexion();
		$data=array();
		$data["region"] = Region::all();
		$data["categories"] = Categorie::all();
		$data["csrftoken"] = $_SESSION["csrf_token"];


		if(isset($_SESSION['user']['id'])){
			$id = $_SESSION['user']['id'];
			$user = User::find($id);
			$data['donnee'] = $user;
			$data['pasmdp'] = true;
			$data["nbAnn"] = Annonce::where('user_id', $user->id)->count();
			$listannonces = Annonce::where('user_id','=',$user->id)->get();
    		$data['annonces'] = (array) current($listannonces);

		}



		if(isset($_SESSION["slim.flash"]["error"])){
			
			$data["donnee"]=$_SESSION["slim.flash"]["error"]["data"];
			$data["erreur"] = $_SESSION["slim.flash"]["error"]["erreur"];	
		}
		
		$v = new \views\User( $data,"view" ) ;
		$v->display() ;
	}

	function add(){
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();
		$data = $app->request->post();
		if(isset($_SESSION['user']['id'])){
			$data["pwd"]="azerty";
		}
    	$v = \models\user::validate($data);

	    if($v["erreur"]!=""){//si il y a des erreurs
	   
	    $v["data"]["pwd"]="";
	    $app->flash("error",$v);
	    $app->redirect("/user/inscription");
	    }
	    else{#Si tout est OK

	    	
	    	$nom = $v["data"]["nom"];

			$prenom = $v["data"]["prenom"];
	
			$login = $v["data"]["login"];



			$email = $v["data"]["email"];
	


			$telephone = $v["data"]["telephone"];
		
			$categorie = $v["data"]["categorie_prefere"];

			$region = $v["data"]["region"];
			

			if(isset($_SESSION["user"]["id"])){
				$id = $_SESSION["user"]["id"];
				$user = User::find($id);
				

				

			}
			else{
				$password = password_hash( $v["data"]["pwd"],PASSWORD_DEFAULT);
				$user = new User;
				$user->mdp = $password;

				


			}
			

			$user->nom = htmlentities($nom);
			$user->prenom = htmlentities($prenom);
			$user->login = htmlentities($login);
			$user->region_id = htmlentities($region);
			$user->categorie_prefere = htmlentities($categorie);
			$user->email = htmlentities($email);
			$user->telephone = htmlentities($telephone);
			$user->role_id = 1;
			

			$user->save();
			
			$_SESSION['user']['login'] = $login;
			$_SESSION['user']['id'] = $user->id;
			$_SESSION['user']['role_id'] = $user->role_id;

			$app->redirect("/");

	    }
	}


	/*fonction pour afficher la page de connection */
	function connection($data=array()) {
		$data["csrftoken"] = $_SESSION["csrf_token"];
		if(isset($_SESSION["slim.flash"]["noConnect"])) {
			$data['noConnect'] = $_SESSION["slim.flash"]["noConnect"];
		}
		/* Chargement de la vue correspondante et affichage */
		$v = new \views\User ( $data, "connection");
		$v->display();
	}
	
	
	/* fonction de validation du log de l utilisateur */
	function check() {
	
		/* si le format des donnes est correct et si l'utilisateur existe
		  et si il y a le bon password, on renvoie sur l index*/
		if ($this->check_format($_POST['users_login'],$_POST['users_passorwd'])
			&& $this->check_login($_POST['users_login'],$_POST['users_passorwd']) ) {
			
		// renvoie sur l'index
		$app = \Slim\Slim::getInstance();
		$app->redirect('/');
		}
		
		/* Sinon on affiche un message d erreur et on le remet sur la de connection*/
		
		$data["error"]= "Les informations transmises n'ont pas permis de vous authentifier.";
		$this->connection($data);

	}
	
	/* fonction qui permet de verifier les information fournis par l utilisateur */
	function check_format($login, $pw){
	
		return true;
	}
	
	/* fonction qui verifie si l'utilsateur existe
		et si le pw est celui de l utilisateur */
	function check_login($login, $pw) {
		// connection au la bbd
		Connection::makeConnexion();
		
		// recherche de l user
		$user = User::where('login',$login)->first();
		
		// si resultat, et verfie pw 
		if ($user != null && password_verify($pw, $user->mdp)) {
			// on creer l'objet user dans la session pour conserve l'utilsateur
			$_SESSION['user']['login'] = $login;
			$_SESSION['user']['id'] = $user->id;
			$_SESSION['user']['role_id'] = $user->role_id;
			return true;
		}
		
		return false;
	}



	/* fonction pour deconnecter l utilisateur*/
	function logout() {
		Connection::makeConnexion();
		// si l'user est en session
		if (isset($_SESSION['user'])) {
			// on enregistre dans la bdd sa dernier conection
			$user = User::find($_SESSION['user']['id']);
			$user->last_log = date("Y-m-d H:i:s");
			$user->save();
			
			// on le supprimer
			unset ($_SESSION['user']);
		}
		
		// renvoie sur l'index
		$app = \Slim\Slim::getInstance();
		$app->redirect('/');
	}

}
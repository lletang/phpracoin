<?php
namespace controllers;

use \confs\Connection;
use \models\annonce;
use \models\user;
use \models\region;
use \models\categorie;
use \models\photoAnnonce as Photo;

use \views;

class AnnonceController {

	function affiche($id=NULL) {
		$app = \Slim\Slim::getInstance();
		Connection::makeConnexion();

		$data=array();
		
		$data["csrftoken"] = $_SESSION["csrf_token"];

		if(!empty($id)) {
			if(!is_numeric($id)) {
				$categories = Categorie::all();
				$match = false;
				foreach ($categories as $cat) {
					if(strtolower($cat->libelle_categorie) == $id) {
						$match = true;
					}
				}
				if($match) {
					//print_r($categories);
					$libelleCateg = strtoupper($id);
					$catID = Categorie::where('libelle_categorie',$id)
					->get();					
					foreach ($catID as $cat) {
						$idCat = $cat->id;
					}
					$annonces = Annonce::where('categorie_id',$idCat)
					->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
					->groupBy("annonces.id")
					->get();
					$data = array("data"=>(array) current($annonces));
					$v = new \views\Annonce( $data,"affiche" );
					$v->display();
				}
				else {
					//print_r($categories);
					$data = array();
					$data["AucuneAnnonce"] = "Aucune annonce ne correspond à cette categorie.";
					$v = new \views\Annonce($data,"affiche") ;
					$v->display();
				}
			}
			else {
				//$annonce = Annonce::whereIn('id', $id)->with('categories')->get();
			
				$photo = Photo::where('annonce_id', '=', $id)->get();

				//$data["categories"] =Annonce::find($id)->categorie;
				//$a = $annonce->load("categories");


				$annonce = Annonce::where('annonces.id',$id)
				->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
				->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
				->get();


				$data['id'] = $id;
				$data["annonce"] =$annonce;
				$data['photo'] = $photo;

				/* prévisualisation de l'annonce */
				$annonceObj = Annonce::find($id);

				unset($data['previsu']);
				unset($data["valide"]);
				unset($data["notvalide"]);

				if ($annonceObj->previsu == 1) {
					$data["notvalide"] = "L'annonce n'a pas encore été validée, elle ne sera pas visible.";
					$data['previsu'] = 1; 
					if ($app->request->post("validprevisu")) {
						$data["valide"] = "L'annonce à été sauvegardée. Elle est maintenant visible par tout le monde.";
						$annonceObj->previsu = 0;
						$annonceObj->save();
						unset($data['previsu']);	
						unset($data["notvalide"]);				
					}
				}
				
			}
			$v = new \views\Annonce($data,"detail") ;

		}
		else {
			
			/* Filtre */
			$app = \Slim\Slim::getInstance();
			$categories = Categorie::all();
			$regions = Region::all();


			if(!empty($app->request->get())){


				if(!empty($app->request->get("categorie_id")) && $app->request->get("categorie_id")!=0)
					$category = $app->request->get("categorie_id");
				else $category= false;

				if(!empty($app->request->get("region_id")))
					$reg = $app->request->get("region_id");
				else $reg = false;

				if(!empty($app->request->get("motCle")))
					$motcle = $app->request->get("motCle");
				else $motcle = false;


				if(!empty($app->request->get("prixMin")) && !empty($app->request->get("prixMax"))){

					
					$prixmin = $app->request->get("prixMin");
					$prixmax = $app->request->get("prixMax");
					
				}
				else{
					$prixmin = false;
					$prixmax = false;

				} 

			
				//var_dump($prixmin);


				
				$annonces = Annonce::where(function($query) use ($category, $reg, $prixmin, $prixmax,$motcle) {
				        if($category){

				            $query->where('categorie_id', $category);
				        }

				        if($reg){
				        	
				            $query->where('region_id', $reg);
				        }
				        
				        if($prixmin && $prixmax){
				        	
				            $query->whereBetween('prix', array($prixmin,$prixmax));
				        }

				       if($motcle){
				       
				       		$query->where('titre','LIKE',"%$motcle%")
				        	->orWhere('descriptif','LIKE',"%$motcle%");

				       }
				            

				        /*if($hotDeal)
				            $query->where('hotDeal', $hotDeal);*/
				    })
					->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
			        ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
			        ->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
			        ->where('annonces.previsu',0)
				    ->orderBy('date', 'DESC')
				    ->groupBy("annonces.id")
				    ->get();

			}
			else{
				$annonces = Annonce::orderBy('date', 'DESC')



				->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
			    ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
			    ->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
			    ->where('annonces.previsu',0)
				->groupBy("annonces.id")
				->get();

			}
			

			$data = array("data"=>(array) current($annonces));
			foreach ($_GET as $key => $value) {
				$data[$key]=$value;
				# code...
			}
			
			$data["regions"]= $regions;
			$data["categories"] = $categories;
			
			/*echo "<pre>";
			print_r($annonces);
			echo "</pre>";*/
			$v = new \views\Annonce( $data,"affiche" );
		}
		
		
		$v->display();
	}

	function afficheMesAnnonces($id=NULL) {
		
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();
		//S'il existe un utilisateur
		if(isset($_SESSION['user'])) {
			if($_SESSION["user"]["role_id"]==1){
				
				//On récupere l'id de l'utilisateur existant 
				$userID = $_SESSION['user']['id'];
				//Et on regarde s'il a déposé des annonces 
				$nbAnnonces = Annonce::where('user_id', '=', $userID)->count();
				$annonces = Annonce::where('user_id', '=', $userID)
				->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
				    ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
				->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
				->groupBy("annonces.id")
				->get();
			}
			else{
				$nbAnnonces = Annonce::count();
				$annonces = Annonce::
				leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
				->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
				->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
				->groupBy("annonces.id")
				->get();
			}
			//Si oui, on prépare l'affichage des annonces
			$data = array("data"=>(array) current($annonces));
			$data["csrftoken"] = $_SESSION["csrf_token"];
			//Si non, on prépare un message d'erreur
			if($nbAnnonces == 0) {
				$data['noAnnce'] = "Vous n'avez pas depose d'annonce sur ce compte utilisateur";
			}		
		}
		else {
			$data['pasCo'] = "Vous n'etes pas connecté";
		}
		if(isset($_GET['mailAnnonce'])) {
				//On récupere le mail
				$mail = $_GET['mailAnnonce'];
				//On vérifie si ce qui a été saisi est bien un email 
				$verifmail="!^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$!";
				//Si c'en est pas un on redirige vers une erreur
				if(!preg_match($verifmail,$mail)) {
					$data['noAnnce'] = "L'adresse mail saisie est incorrecte.";
				}
				//Sinon on va chercher les annonces
				else {
					//On regarde s'il existe des annonces qui correspondent en base de donnée
					$nbAnnoncesWithMail = Annonce::where('email', '=', $mail)->count();
					$annoncesWithMail = Annonce::where('email', '=', $mail)
					->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
			        ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
					->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
					->groupBy("annonces.id")
					->get();
					//Si oui, on prépare l'affichage des annonces
					$data = array("data"=>(array) current($annoncesWithMail));
					//Si non, on prépare un message d'erreur
					if($nbAnnoncesWithMail == 0) {
						$data['noAnnce'] = "Aucune annonce ne correspond à cette adresse mail.";
					}
				}
			}
			$v = new \views\Annonce( $data,"mesannonces" ) ;
			$v->display() ;
	}


	function add() {
		Connection::makeConnexion();
		$data = array();
		$categorie = Categorie::all();
		$region = Region::all();
		$data["categories"] = $categorie;
		$data["regions"] = $region;
		$data["csrftoken"] = $_SESSION["csrf_token"];

		if (!empty($_SESSION['slim.flash']['flashErreur'])) {
			$data["slim"] = $_SESSION['slim.flash']['flashErreur'];
		}
		

		$id = null;

		if (!empty($_SESSION['user'])) {
			$id = $_SESSION['user']['id'];
			$user = User::find($id);
			$data['data'] = $user;
		}

		$v = new \views\Annonce($data ,"ajout");
		$v->display();
		
	}

	function edit($id = null) {

		Connection::makeConnexion();

		$app = \Slim\Slim::getInstance();
		$post = $app->request->post();

		$data=array();
		$data["csrftoken"] = $_SESSION["csrf_token"];

		$region= Region::all();
		$data["regions"] = $region;

		if(isset($id)) {
			$annonce = Annonce::find($id);
			$data["id"]=$annonce->id;

			$categorie= Categorie::all();
			$data["categories"] = $categorie;

			if($annonce!=NULL){

				if(isset($post) && !empty($post)){
					
					if($post["mail"]==$annonce->email){

						if(password_verify($post["password"],$annonce->mdp)){
							$_SESSION['user_tmp']['email']= $annonce->email;
							$_SESSION['user_tmp']['annonce_id']= $annonce->id;
							$data["slim"]["datas"]=$annonce;
							$v = new \views\Annonce( $data,"ajout" ) ;

							$v->display() ;
							
							
						}
						else{
							$data["formulaire"] = true;
							$data['error'] = "L'identifiant ou le mot de passe est incorrect";


						}
					}
					else{
						$data["formulaire"] = true;
						$data['error'] = "L'identifiant ou le mot de passe est incorrect, ou vous n'êtes pas connecté";
					}
			
				}
				else if(isset($_SESSION['user'])){

					if($_SESSION["user"]["role_id"]==1){
						$data["slim"]["datas"]=$annonce;
						$data["data"]["id"] = $annonce->user_id;
						
						$v = new \views\Annonce( $data,"ajout" ) ;

						$v->display() ;
					}
					else if($_SESSION["user"]['id']==$annonce->user_id){
						$data["slim"]["datas"]=$annonce;
						$data["data"]["id"] = $annonce->user_id;
						
							$v = new \views\Annonce( $data,"ajout" ) ;

							$v->display() ;
						#c'est le bon utilisateur
					}
					else{
						$data["formulaire"] = true;
						$data["error"] = "Vous n'êtes pas autorisé à modifier cette annonce";
						#c'est pas le bon utilisateur
					}
				}
				else if(isset($_SESSION['user_tmp']['email']) && isset($_SESSION['user_tmp']['annonce_id']) && $_SESSION['user_tmp']['annonce_id']==$annonce->id){
					if(isset($_SESSION["user"]))
						$_SESSION["user_tmp"]= array();

					else if($_SESSION['user_tmp']['email']==$annonce->email){
						$data["slim"]["datas"]=$annonce;
							$v = new \views\Annonce( $data,"ajout" );

							$v->display() ;
						#c'est le bon utilisateur
					}
					else{
						$data["error"] = "Vous n'êtes pas autorisé à modifier cette annonce";
						#c'est pas le bon utilisateur
					}



				}
				else{#pas d'utilisateur valide temporaire

					$data["error"] = "Vous devez rentrer les identifiants de l'annonce pour pouvoir la modifier";
					$data["formulaire"] = true;
				}

			}
			else{#l'annonce n'existe pas
				$data["error"] = "L'annonce n'éxiste pas";
			}
			$data["lien"]= "/annonces/edit/".$id;
			$v = new \views\Annonce( $data,"logininvites" ) ;

			$v->display() ;
	
		}
		else echo "l'id ne correspond à aucune annonce";

	}


	function delete($id=NULL)
	{
		Connection::makeConnexion();

		$app = \Slim\Slim::getInstance();
		$post = $app->request->post();



		$data=array();
		$data["csrftoken"] = $_SESSION["csrf_token"];
		if(isset($id)) {
			$annonce = Annonce::find($id);
			$data["id"]=$annonce->id;

			$categorie= Categorie::all();
			$data["categories"] = $categorie;




			
			if($annonce!=NULL){



				if(isset($post) && !empty($post)){
					
					if($post["mail"]==$annonce->email){
						if(password_verify($post["password"],$annonce->mdp)){
							$_SESSION['user_tmp']['email']= $annonce->email;
							$_SESSION['user_tmp']['annonce_id']= $annonce->id;
							$annonce->delete();
							$app->redirect("/");
							
							
						}
						else{

							$data['error'] = "l'identifiant ou le mot de passe est incorrecte";

						}
					}
					else{
						$data['error'] = "l'identifiant ou le mot de passe est incorrecte";
					}
			
				}
				else if(isset($_SESSION['user'])){
					if($_SESSION["user"]["role_id"]==2){
						$annonce->delete();
						$app->redirect("/");
					}
					if($_SESSION["user"]['id']==$annonce->user_id){
						
						$annonce->delete();
						$app->redirect("/");
						
						#c'est le bon utilisateur
					}
					else{
						$data["error"] = "Vous n'êtes pas autorisé à modifier cette annonce";
						#c'est pas le bon utilisateur
					}
				}
				else if(isset($_SESSION['user_tmp']['email']) && isset($_SESSION['user_tmp']['annonce_id']) && $_SESSION['user_tmp']['annonce_id']==$annonce->id){
					if($_SESSION['user_tmp']['email']==$annonce->email){
						
							$annonce->delete();
							$app->redirect("/");
						#c'est le bon utilisateur
					}
					else{
						$data["error"] = "Vous n'êtes pas autorisé à modifier cette annonce";
						#c'est pas le bon utilisateur
					}



				}
				else{#pas d'utilisateur valide temporaire

					$data["error"] = "Vous devez rentrer les identifiants de l'annonces pour pouvoir la modifer";
					$data["formulaire"] = true;
				}

			}
			else{#l'annonce n'existe pas
				$data["error"] = "l'annonce n'exite pas";
			}
			$data["lien"]= "/annonces/delete/".$id;
			$v = new \views\Annonce( $data,"logininvites" ) ;

			$v->display() ;
			
			
				
			
		}




	}

	function search() 
	{

		$app = \Slim\Slim::getInstance();

		foreach ($app->request->get() as $key => $value) {
			echo $key.' : '.$value;
		}
	}

	function ajoute($idAnnonce = null) 
	{

		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();

		$data = $app->request->post();
		$dataImage = null;

		foreach ($_FILES as $key => $value) {

			if (!empty($_FILES[$key]['tmp_name'])) {
				$dataImage[$key] = $_FILES[$key];
			}
		}

		if ($dataImage == null)
			$defaultUrlImg = './img/img.jpg';

		$v = Annonce::validate($data, $dataImage);

		if (!empty($v["erreur"])) {
			if (!empty($v['datas']['password']))
		    	$v["datas"]["password"]= "";
		    $app->flash("flashErreur",$v);
		    $app->redirect("/annonces/new");
		}
		else {

			$titre = $data['titre'];
			$descriptif = $data['descriptif'];
			$categorie = $data['categorie'];
			$region = $data['region'];
			$telephone = $data['telephone'];
			$email = $data['email'];
			$prix = $data['prix'];
			if (!empty($data['password']))
				$mdp = $data['password'];

			$id = 0;

			if (!empty($_SESSION['user'])) {
				$id = $_SESSION['user']['id'];
			}

			if (!empty($idAnnonce))
				$annonce = Annonce::find($idAnnonce);
			else
				$annonce = new Annonce();

			$annonce->user_id = $id;
			
			$annonce->categorie_id = $categorie;
			if (!empty($mdp))		
				$annonce->mdp = password_hash($mdp, PASSWORD_DEFAULT);

			$annonce->telephone = $telephone;
			$annonce->region_id = $region;
			$annonce->titre = $titre;
			$annonce->prix = $prix;
			$annonce->descriptif = $descriptif;
			$annonce->email = $email;
			$annonce->categorie_id = $categorie;	

			if(!empty($idAnnonce))	
				$annonce->previsu = 0;
			else
				$annonce->previsu = 1;

			$annonce->save();

			if(!empty($idAnnonce)){
				$photodel = Photo::where('annonce_id', $annonce->id)->delete();

				if (isset($defaultUrlImg)) {
					$photo = new Photo();

					$photo->url_photo = $defaultUrlImg;
					$photo->annonce_id = $annonce->id;
					$photo->save();
				}
			}

			if (isset($defaultUrlImg)) {
				$photodel = Photo::where('annonce_id', $annonce->id)->delete();
				$photo = new Photo();

				$photo->url_photo = $defaultUrlImg;
				$photo->annonce_id = $annonce->id;
				$photo->save();
			}
			else {
				foreach ($_FILES as $key => $value) {
					$name = $key.'_name';
					$$name = $_FILES[$key]["name"]; 
					$size = $key.'_size';
					$$size = $_FILES[$key]["size"];
					$tmpName = $key.'_tmp';
					$$tmpName = $_FILES[$key]["tmp_name"];
					$extension_upload = $key.'_ext';
					$$extension_upload = strtolower(substr(strrchr($$name, '.') ,1));
					$namePhoto = $key.'_rename';
					$$namePhoto = time().hash("sha256", $$tmpName).".".$$extension_upload;

					move_uploaded_file($$tmpName, sprintf('./upload/%s', $$namePhoto));

					$photo = $key.'_photo';
					$$photo = new Photo();

					$$photo->url_photo = "./upload/".$$namePhoto;
					$$photo->annonce_id = $annonce->id;
					$$photo->save();
				} 
			}
			$app->redirect("/annonces/$annonce->id");
		}				
	}
}

<?php

namespace controllers;

use \confs\Connection;
use \models\roles;
use \models\user;
use \models\region;
use \models\categorie;
use \models\annonce;
use \views;



class AdminController {

	function void(){
		$v = new \views\Admin( array(),"view" ) ;
		$v->display() ;
	}

	function viewUser($id) {
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();

		if(!empty($_SESSION['user']['role_id']) && $_SESSION['user']['role_id'] == 2) {

			$data=array();
			$data["region"] = Region::all();
			$data["categories"] = Categorie::all();
			$data["csrftoken"] = $_SESSION["csrf_token"];

			$user = User::find($id);
			$data['donnee'] = $user;
			$data['pasmdp'] = true;
			$data["nbAnn"] = Annonce::where('user_id', $user->id)->count();
			$listannonces = Annonce::where('user_id','=',$user->id)->get();
			$data['annonces'] = (array) current($listannonces);
			
			$v = new \views\User( $data,"view" ) ;
			$v->display() ;
		}
		else
			$app->redirect("/");
	}
	

	function afficherForm(){
		
		Connection::makeConnexion();
		
		//recuperation de tous les roles
		$roles = Roles::all();
		$data["roles"] = $roles;
		$data['regions'] = Region::all();
		$data['categories']= Categorie::all();
		
		$data["csrftoken"] = $_SESSION["csrf_token"];
		
		if(isset($_SESSION["slim.flash"]["error"])){		
			$data["donnee"]=$_SESSION["slim.flash"]["error"]["data"];
			$data["erreur"] = $_SESSION["slim.flash"]["error"]["erreur"];
		}
		
		if(isset($_SESSION["addusersucces"])) {
			$data["succes"] = $_SESSION["addusersucces"];
			unset( $_SESSION["addusersucces"]);
		}

		$data["nbUser"] = User::all()->count();
		$listUser = User::all();
    	$data['listUser'] = (array) current($listUser);

		$v = new \views\Admin( $data,"adduser" ) ;
		$v->display() ;
	}
	
	
	function adduser() {
	
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();
		$data = $app->request->post();
    	$v = \models\user::validate($data);
		
		$data["csrftoken"] = $_SESSION["csrf_token"];
		
		
		// test sur les erreur du validator user
		if($v["erreur"]!=""){
	   
			$v["data"]["pwd"]="";
			$app->flash("error",$v);
			$app->redirect("/admin/gestionuser");
	    }
		
		// si le validator est passe
	    else{

	    	
	    	$nom = $v["data"]["nom"];
			$prenom = $v["data"]["prenom"];
			$login = $v["data"]["login"];
			$password = password_hash( $v["data"]["pwd"],PASSWORD_DEFAULT);
			$email = $v["data"]["email"];
			$telephone = $v["data"]["telephone"];
			$categorie = $v["data"]["categorie_prefere"];			
			$region = $v["data"]["region"];		
			$role = $v["data"]["role"];
	
			$user = new User;
			
			$user->mdp = $password;

			$user->nom = htmlentities($nom);
			$user->prenom = htmlentities($prenom);
			$user->login = htmlentities($login);
			$user->region_id = htmlentities($region);
			$user->categorie_prefere = htmlentities($categorie);
			$user->email = htmlentities($email);
			$user->telephone = htmlentities($telephone);
			$user->role_id = htmlentities($role);

			$user->save();
			
			$_SESSION["addusersucces"]= "L'utilisateur est bien enregistré";
			
			$app->redirect("/admin/gestionuser");

	    }
	}
	
	function afficherGestionCate($data = array()) {
		Connection::makeConnexion();
		
		//recuperation de toutes les cate
		$cates = Categorie::all();
		$data["cates"] = $cates;
		
		$data["csrftoken"] = $_SESSION["csrf_token"];
		
		$v = new \views\Admin( $data,"addcate" ) ;
		$v->display() ;
	}
	
	function addcate() {
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();
		$data = $app->request->post();
		$data['selected'] = $data['cate'];
		
		$data["csrftoken"] = $_SESSION["csrf_token"];
		
		if($this->check_format_libelle($data['libelle']) ) {
			
			
			if ($data['selected'] == 0 ) {
				//sauvegardede la cate new cate
				$cate = new Categorie;
				$cate->libelle_categorie = $data['libelle'];
				
				$cate->save();
				
				$data['selected'] = Categorie::where('libelle_categorie', $data['libelle'])->first()->id;
				$data["message_gestion_cate_succes"] = "Categorie enregistrer";
			}
			
			else {
				// modification cate 
				$cate = Categorie::find($data['cate']);
				
				$cate->libelle_categorie = $data['libelle'];
				$cate->save();
				
				$data["message_gestion_cate_succes"] = "Categorie modifier";
			}
			
		}
		else {
			$data["message_gestion_cate_error"] = "Entreer invalide";
		}
		
		$this->afficherGestionCate($data);
	}	
	
	//check du format
	function check_format_libelle($libelle) {
		return true;
	}
	
	function suppcate() {
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();
		$data = $app->request->post();
		
		$data["csrftoken"] = $_SESSION["csrf_token"];
		
		// on verifie qu'on trouve une categorie lie au numero selection
		if ($cate = Categorie::find($data['numero_suppresion'])) {
			$cate->delete();
			$data["message_gestion_cate_succes"] = "Categorie supprimer";
		}
		else {
			$data["message_gestion_cate_error"] = "Erreur dans la suppresion";
		}
		
		$this->afficherGestionCate($data);
	}
	
	
	
	function viewfreshannonce($data = array()) {
		Connection::makeConnexion();
		
		// on recupere la dernier date de connection de admin
		$last_log = User::find($_SESSION['user']['id'])->last_log;
		
		// on recuperre toutes les anonces avec une date supperieur
		$nbannonces = Annonce::where('date', '>', $last_log)->count();
		$listannonces = Annonce::where('date', '>', $last_log)
					->leftJoin('categories', 'annonces.categorie_id', '=', 'categories.id')
			        ->leftJoin('regions', 'annonces.region_id', '=', 'regions.id')
					->leftJoin("photos_annonces","annonces.id","=","photos_annonces.annonce_id")
					->groupBy("annonces.id")
					->get();		
		//Si oui, on prépare l'affichage des annonces
		$data['annonces'] = (array) current($listannonces);
		// si non, on affiche une erreur.
		if($nbannonces == 0) {
			$data['noAnnce'] = "Aucune nouvelle annonce depuis la dernier connection.";
		}
		$v = new \views\Admin( $data,"viewfreshannonce" ) ;
		$v->display() ;
	
	
	}
	
	function deleteAdmin($id=NULL)
	{
		Connection::makeConnexion();
		$app = \Slim\Slim::getInstance();

		if($annonce = Annonce::find($id)) {
			$annonce->delete();
			$data['succes']="Annonce supprimer";
		}
		else {
			$data['error'] = "Une erreur est survenue";
		}
		
		
		$this->viewfreshannonce($data);
	}
	
}
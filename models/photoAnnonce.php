<?php

namespace models;

class PhotoAnnonce extends \Illuminate\Database\Eloquent\Model  {
 
	protected $table = 'photos_annonces';
	protected $primaryKey = 'id';
	public $timestamps=false;

}

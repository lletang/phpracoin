<?php

namespace models;
use Respect\Validation\Validator as v;

class Annonce extends \Illuminate\Database\Eloquent\Model  {
 
	protected $table = 'annonces';
	protected $primaryKey = 'id';
	public $timestamps=false;


	public function categories() {
	    return $this->belongsTo('\models\categorie');
	}

	public function getUserAnnonce($id) {
    	return $this->belongsTo($id);
    }
	

	public static function validate($data, $dataImage) {

		$titre = $data['titre'];
		$descriptif = $data['descriptif'];
		$telephone = $data['telephone'];
		$categorie = $data['categorie'];
		$region = $data['region'];
		$email = $data['email'];
		$prix = $data['prix'];

		if(preg_match("#api#", $_SERVER['REQUEST_URI']))
			$mdp = $data['password'];

		if (!empty($data['password']))
			$mdp = $data['password'];

		$error = array ();


		$validTitre = v::notEmpty()->length(5,50)->validate($titre);
		if (!$validTitre) {
			$msgvalidTitre ="Le titre est trop court ou trop long.";
			$error['msgvalidTitre'] = $msgvalidTitre;
		}

		$validDescription = v::notEmpty()->length(15,2000)->validate($descriptif);
		if (!$validDescription) {
			$msgvalidDescription ="La description est trop courte.";
			$error['msgvalidDescription'] = $msgvalidDescription;
		}

		$validCategorie = v::notEmpty()->int()->validate($categorie);
		if (!$validCategorie) {
			$msgvalidCategorie ="La catégorie n'est pas correcte.";
			$error['msgvalidCategorie'] = $msgvalidCategorie;
		}

		$validRegion = v::notEmpty()->int()->validate($region);
		if (!$validRegion) {
			$msgvalidRegion ="La région n'est pas correcte.";
			$error['msgvalidRegion'] = $msgvalidRegion;
		}

		$validTelephone = v::notEmpty()->noWhitespace()->length(10,10)->validate($telephone);
		if (!$validTelephone) {
			$msgvalidTelephone ="Le numéro de téléphone doit comporter 10 chiffres, sans espace ni séparateur.";
			$error['msgvalidTelephone'] = $msgvalidTelephone;
		}

		$validMail = v::notEmpty()->email()->validate($email);
		if (!$validMail) {
			$msgvalidMail ="Le format de l'adresse email n'est pas valide.";
			$error['msgvalidMail'] = $msgvalidMail;
		}

		$validPrix = v::notEmpty()->int()->validate($prix);
		if (!$validPrix) {
			$msgvalidPrix ="Le prix contient des caractères non autorisés.";
			$error['msgvalidPrix'] = $msgvalidPrix;
		}

		if (!empty($mdp) || preg_match("#api#", $_SERVER['REQUEST_URI'])) {
			$validPassword= v::notEmpty()->length(8,16)->validate($mdp);
				if (!$validPassword) {
					$msgvalidPassword ="Le mot de passe n'est pas valide, il doit contenir entre 8 et 16 caractères.";
					$error['msgvalidPassword'] = $msgvalidPassword;
				}			
		}


		/******  Vérification upload image ******/
		if ($dataImage != null) {

			// taille max de l'image
			$max_file_size = 10048576; // 10 Mio

			$_FILES = array ();
			$_FILES = $dataImage;

			$nbIMG = count($_FILES);
			$extensions_valides = array('jpg', 'jpeg', 'png', 'gif', 'bmp');

			foreach ($_FILES as $key => $value) {

				$name = $key.'_name';
				$$name = $_FILES[$key]["name"]; 
				$size = $key.'_size';
				$$size = $_FILES[$key]["size"];
				$tmpName = $key.'_tmp';
				$$tmpName = $_FILES[$key]["tmp_name"];
				$extension_upload = $key.'_ext';
				$$extension_upload = strtolower(substr(strrchr($$name, '.') ,1));

				if (!in_array($$extension_upload,$extensions_valides))
					$error["msgvalidImage"] = 'Extension du fichier non correcte.';
				if ($$size > $max_file_size) 
					$error["msgvalidImage"] = 'Fichier trop volumineux.';

			}

		}

		$result["datas"]= $data;
	    $result["erreur"]= $error;
	    	
	    return $result;
	}


	public function photo()
    {
        return $this->belongsToMany('\models\photoAnnonce');
    }

}

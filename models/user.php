<?php

namespace models;
use Respect\Validation\Validator as v;


class User extends \Illuminate\Database\Eloquent\Model  {
 
 protected $table = 'users';
 protected $primaryKey = 'id';
 public $timestamps=false;

	public static function validate($data) {

		 

	    $nom=$data["nom"];
	    $login = $data["login"];
	    $prenom=$data["prenom"];
	    $telephone = $data["telephone"];
	    $password = $data["pwd"];
	    $region=$data["region"];
	    $email=$data["email"];

	    $erreur="";
	   $validlogin = v::alnum()
                  		->noWhitespace()
                  		->length(1,15)
                  		->validate($login);

        if(!$validlogin)
        	$erreur.="Le login ne doit pas contenir d'espace et doit contenir au minimum 1 caractères et au maximum 15 caractères<br />";

         $validemail = v::email()
                  		->validate($email);

         if(!$validemail)
        	$erreur.="L'adresse email est invalide<br />";

         $validnom = v::string()
                  		->length(1,25)
                  		->validate($nom);

        if(!$validnom)
        	$erreur.="Le nom doit être une chaine de caractère entre 1 et 25 caractères<br />";

         $validprenom = v::string()
                  		->length(1,25)
                  		->validate($prenom);
        if(!$validprenom)
        	$erreur.="Le prénom doit être une chaine de caractère entre 1 et 25 caractères<br />";


         $validtelephone = v::phone()
                  		->validate($telephone);


        if(!$validtelephone)
        	$erreur.="Le numéro de téléphone est invalide<br />";

	  
	    $result["data"]=$data;
	    $result["erreur"]=$erreur;
	    	
	    return $result;

				
	}

}
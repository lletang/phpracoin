<?php 
namespace confs;
use Illuminate\Database\Capsule\Manager as Capsule;
class Connection{


	public static function makeConnexion()
	{

	$capsule = new Capsule;

	$capsule->addConnection(parse_ini_file("confs/conf.ini")
	    
	);
	
	// Make this Capsule instance available globally via static methods... (optional)
	$capsule->setAsGlobal();

	// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
	$capsule->bootEloquent();

	
	}
}
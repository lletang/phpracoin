-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 07 Février 2015 à 19:55
-- Version du serveur: 5.5.41-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `racoin`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

CREATE TABLE IF NOT EXISTS `annonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `prix` double NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descriptif` mediumtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `mdp` varchar(256) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `pseudo` varchar(256) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `previsu` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Contenu de la table `annonces`
--

INSERT INTO `annonces` (`id`, `titre`, `prix`, `date`, `descriptif`, `user_id`, `email`, `mdp`, `categorie_id`, `region_id`, `pseudo`, `telephone`, `previsu`) VALUES
(77, 'Ordinateur portable de jeu Alienware', 2248, '2015-02-07 18:11:34', 'Découvrez le nouvel ordinateur Alienware 18, avec sa conception entièrement repensée à deux cartes graphiques pour une expérience de jeu sans précédent. Avec Windows 8.1 et cartes graphiques NVIDIA® GeForce® GTX 860M et GTX 880M.', 11, 'neo@lamatrice.com', '', 3, 1, '', '7777777777', 0),
(78, 'Tennis basses Nalo', 50, '2015-02-07 18:18:32', 'Chaussures d''entraînement et de fitness ', 11, 'neo@lamatrice.com', '', 4, 1, '', '7777777777', 0),
(79, 'Meubles en bois 2 portes', 9500, '2015-02-07 18:30:44', 'Un petit meuble pas trop cher je ne m''en sert plus alors je le vend', 10, 'admin@gmail.com', '', 4, 15, '', '0383202020', 0),
(80, 'Donne cours de PHP aux noob', 550800, '2015-02-07 18:33:46', 'Bonjour, étant donné la recrudescence affolante de jeunes développeurs qui na savent pas coder, je propose de donner des cours de PHP à des prix exorbitant. \r\n\r\n---- PS: ne me contactez surtout pas si vous en avez besoin ----', 12, 'johndoe@gmail.com', '', 6, 6, '', '0698788596', 0);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_categorie` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `libelle_categorie`) VALUES
(1, 'VEHICULES'),
(2, 'IMMOBILIER'),
(3, 'MULTIMEDIA'),
(4, 'MAISON'),
(6, 'EMPLOI & SERVICE'),
(7, 'AUTRE'),
(8, 'LOISIR');

-- --------------------------------------------------------

--
-- Structure de la table `photos_annonces`
--

CREATE TABLE IF NOT EXISTS `photos_annonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annonce_id` int(11) NOT NULL,
  `url_photo` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

--
-- Contenu de la table `photos_annonces`
--

INSERT INTO `photos_annonces` (`id`, `annonce_id`, `url_photo`) VALUES
(146, 78, './upload/14233334586e78aaf32bae735cc95cf39a3e78993b99bc1a74fc9ab93fcf3b2f5154f9dea0.jpg'),
(147, 79, './upload/1423333844c6be2eaf48f651bacba6257eaeff73b07737390a0577e6aa069f8e4e4fa6664b.jpg'),
(148, 80, './img/img.jpg'),
(150, 77, './upload/14233352707eeff9f37322fe442e6657f46e72bf7857147433cf1f2c9674e4c85cbe8528da.jpg'),
(151, 77, './upload/1423335270fff2c7bda02257ce9e6476e3da8b4e8ef854b162e49957dcaa5cea93857f8652.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_region` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `regions`
--

INSERT INTO `regions` (`id`, `libelle_region`) VALUES
(1, 'Alsace'),
(2, 'Aquitaine'),
(3, 'Auvergne'),
(4, 'Basse Normandie'),
(5, 'Bourgogne'),
(6, 'Bretagne'),
(7, 'Centre'),
(8, 'Champagne Ardenne'),
(9, 'Corse'),
(10, 'Franche Comte'),
(11, 'Haute Normandie'),
(12, 'Ile de France'),
(13, 'Languedoc Roussillon'),
(14, 'Limousin'),
(15, 'Lorraine'),
(16, 'Midi-Pyrénées'),
(17, 'Nord Pas de Calais'),
(18, 'Provence Alpes Côte d''Azur'),
(19, 'Pays de la Loire'),
(20, 'Picardie'),
(21, 'Poitou Charente'),
(22, 'Rhone Alpes');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_role` varchar(256) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `libelle_role`, `level`) VALUES
(1, 'user', 1),
(2, 'admin', 5);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `login` varchar(256) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `email` varchar(256) NOT NULL,
  `mdp` varchar(256) NOT NULL,
  `role_id` varchar(256) NOT NULL,
  `categorie_prefere` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `derniere_connexion` datetime NOT NULL,
  `last_log` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `login`, `telephone`, `email`, `mdp`, `role_id`, `categorie_prefere`, `region_id`, `derniere_connexion`, `last_log`) VALUES
(10, 'admin', 'admin', 'admin', '0383202020', 'admin@gmail.com', '$2y$10$XqW81qDh9q1jRtYqGgC7..O3DtFG9L6lepMAlvtvwjcSE2uLdG.N.', '2', 3, 15, '0000-00-00 00:00:00', '2015-02-07 19:31:01'),
(11, 'Anderson', 'Thomas', 'neo', '7777777777', 'neo@lamatrice.com', '$2y$10$8LzKReuvFC63TVnzGHRsL..D2lCozStw5X2M61F8ypNsMeLEkCWju', '1', 1, 1, '0000-00-00 00:00:00', '2015-02-07 19:44:36'),
(12, 'Doe', 'John', 'johndoe', '0698788596', 'johndoe@gmail.com', '$2y$10$LNyT8.rL5z8O8xku22s2.uAA/m/cLfT4W9YmQC4JtQrXIrjJMzmsy', '1', 6, 6, '0000-00-00 00:00:00', '2015-02-07 19:37:19');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

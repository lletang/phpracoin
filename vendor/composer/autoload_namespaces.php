<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'views' => array($baseDir . '/'),
    'models' => array($baseDir . '/'),
    'controllers' => array($baseDir . '/'),
    'confs' => array($baseDir . '/'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Slim' => array($vendorDir . '/slim/slim'),
    'Negotiation' => array($vendorDir . '/willdurand/negotiation/src'),
    'Illuminate\\Support' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Events' => array($vendorDir . '/illuminate/events'),
    'Illuminate\\Database' => array($vendorDir . '/illuminate/database'),
    'Illuminate\\Container' => array($vendorDir . '/illuminate/container'),
    'Carbon' => array($vendorDir . '/nesbot/carbon/src'),
);
